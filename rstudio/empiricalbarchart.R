library(ggplot2)
library(tikzDevice)
options(tikzPdftexWarnUTF = F)
options(tikzDocumentDeclaration = "\\documentclass{beamer}")

set.seed(73824301)
dataset <- sample.int(6, 20, replace=TRUE, prob=rep(1, 6))

tikz(paste0("barchartdata", 0, ".tex"), width=3, height=2)
ggplot(data=data.frame(x=dataset), aes(x=x)) + 
  geom_bar() + 
  scale_x_discrete(limits=1:6) + 
  labs(y="Antal") +
  theme(axis.title.x = element_blank())
dev.off()

for(i in 1:6) {
  tikz(paste0("barchartdata", i, ".tex"), width=3, height=2)
  plot(ggplot(data=data.frame(x=dataset), aes(x=x, fill=x==i)) + 
    geom_bar() + 
    scale_x_discrete(limits=1:6) + 
    scale_fill_grey(guide=F) +
    labs(y="Antal") +
    theme(axis.title.x = element_blank()))
  dev.off()
}
