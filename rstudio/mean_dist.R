library(ggplot2)
library(tikzDevice)
options(tikzPdftexWarnUTF = F)
options(tikzDocumentDeclaration = "\\documentclass{beamer}")

vikter <- c(1, 2, 15, 100)
dist <- data.frame(Vikt = c(1, 2, 15, 100),
                   Sannolikhet = c(0.5, 0.25, 0.2, 0.05))

tikz("mean_dist1.tex", width = 3.5, height = 2.2)
ggplot(data=dist, aes(x=factor(Vikt), y=Sannolikhet)) +
  geom_bar(stat="identity") + labs(x="Paketvikt (kg)")
dev.off()

tikz("mean_dist2.tex", width = 3.5, height = 2.2)
ggplot(data=dist, aes(x=Vikt, y=Sannolikhet)) +
  geom_bar(stat="identity") + labs(x="Paketvikt (kg)")
dev.off()

tikz("mean_dist3.tex", width = 3.5, height = 2.2)
ggplot(data=dist, aes(x=Vikt, y=Sannolikhet)) +
  geom_bar(stat="identity") + labs(x="Paketvikt (kg)") +
  geom_vline(xintercept=sum(dist$Vikt * dist$Sannolikhet), colour="red")
dev.off()

set.seed(8429)
data.set1 <- data.frame(Vikt = sample(100, x = dist$Vikt, prob = dist$Sannolikhet, replace = T))
data.set2 <- data.frame(Vikt = sample(100, x = dist$Vikt, prob = dist$Sannolikhet, replace = T))
data.set3 <- data.frame(Vikt = sample(100, x = dist$Vikt, prob = dist$Sannolikhet, replace = T))
data.set4 <- data.frame(Vikt = sample(100, x = dist$Vikt, prob = dist$Sannolikhet, replace = T))
data.rest <- data.frame(Vikt = colMeans(replicate(996, sample(100, x = dist$Vikt, prob = dist$Sannolikhet, replace = T))))

tikz("mean_data1.tex", width = 3.5, height = 2.2)
ggplot(data=data.set1, aes(x=Vikt)) +
  geom_bar() + labs(x="Paketvikter lass 1 (kg)", y="Antal") +
  geom_vline(xintercept = mean(data.set1$Vikt), colour="red") + 
  coord_cartesian(xlim=c(0, 100), ylim=c(0, 60))
dev.off()

tikz("mean_data2.tex", width = 3.5, height = 2.2)
ggplot(data=data.set2, aes(x=Vikt)) +
  geom_bar() + labs(x="Paketvikter lass 2 (kg)", y="Antal") +
  geom_vline(xintercept = mean(data.set1$Vikt), colour="green") +
  geom_vline(xintercept = mean(data.set2$Vikt), colour="red") + 
  coord_cartesian(xlim=c(0, 100), ylim=c(0, 60))
dev.off()

tikz("mean_data3.tex", width = 3.5, height = 2.2)
ggplot(data=data.set3, aes(x=Vikt)) +
  geom_bar() + labs(x="Paketvikter lass 3 (kg)", y="Antal") +
  geom_vline(xintercept = mean(data.set1$Vikt), colour="green") +
  geom_vline(xintercept = mean(data.set2$Vikt), colour="green") + 
  geom_vline(xintercept = mean(data.set3$Vikt), colour="red") + 
  coord_cartesian(xlim=c(0, 100), ylim=c(0, 60))
dev.off()

tikz("mean_data4.tex", width = 3.5, height = 2.2)
ggplot(data=data.set4, aes(x=Vikt)) +
  geom_bar() + labs(x="Paketvikter lass 1000 (kg)", y="Antal") +
  geom_vline(xintercept = mean(data.set1$Vikt), colour="green") +
  geom_vline(xintercept = mean(data.set2$Vikt), colour="green") + 
  geom_vline(xintercept = mean(data.set3$Vikt), colour="green") + 
  geom_vline(data=data.rest, aes(xintercept=Vikt), colour="green") +
  geom_vline(xintercept = mean(data.set4$Vikt), colour="red") + 
  coord_cartesian(xlim=c(0, 100), ylim=c(0, 60))
dev.off()

data.all <- rbind(data.rest, data.frame(Vikt = c(mean(data.set1$Vikt), mean(data.set2$Vikt), mean(data.set3$Vikt), mean(data.set4$Vikt))))

tikz("mean_data5.tex", width = 3.5, height = 2.2)
ggplot(data=data.all, aes(x=Vikt)) +
  geom_histogram(bins=22) +
  geom_vline(xintercept=sum(dist$Vikt * dist$Sannolikhet), colour="red")
dev.off()

tikz("mean_data6.tex", width = 3.5, height = 2.2)
ggplot(data=data.all, aes(x=Vikt)) +
  geom_histogram(bins=22) +
  geom_vline(xintercept=sum(dist$Vikt * dist$Sannolikhet), colour="red") + 
  stat_function(fun = function(x) 650*dnorm(x, mean=mean(data.all$Vikt), sd=sd(data.all$Vikt)), colour="red", size=2)
dev.off()
