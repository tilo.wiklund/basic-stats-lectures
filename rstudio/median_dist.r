library(ggplot2)
library(tikzDevice)
options(tikzPdftexWarnUTF = F)
options(tikzDocumentDeclaration = "\\documentclass{beamer}")

dist <- data.frame(Svar = ordered(c("Jättedålig", "dålig", "Mellan", "bra", "Jättebra"), levels=c("Jättedålig", "dålig", "Mellan", "bra", "Jättebra")),
                   Sannolikhet = c(0.22, 0.195, 0.195, 0.195, 0.195))

tikz("median_dist1.tex", width = 3.5, height = 2.2)
ggplot(data=dist, aes(x=Svar, y=Sannolikhet)) +
  geom_bar(stat="identity") +
  theme(axis.text.x = element_text(vjust=1, hjust=0, angle=-45))
dev.off()

dist2 <- data.frame(Svar = ordered(c("Jättedålig", "dålig", "Mellan", "bra", "Jättebra"), levels=c("Jättedålig", "dålig", "Mellan", "bra", "Jättebra")),
                   Sannolikhet = c(0.3, 0.25, 0.1, 0.1, 0.05))

tikz("median_dist2.tex", width = 3.5, height = 2.2)
ggplot(data=dist2, aes(x=Svar, y=Sannolikhet)) +
  geom_bar(stat="identity") +
  theme(axis.text.x = element_text(vjust=1, hjust=0, angle=-45))
dev.off()

set.seed(78474824)
data.set <- data.frame(Svar = ordered(sample(size=100, x=dist$Svar, prob=dist$Sannolikhet, replace=T), levels=c("Jättedålig", "dålig", "Mellan", "bra", "Jättebra")))

sizes <- c("Jättedålig", "dålig", "Mellan", "bra", "Jättebra")

tikz("median_data1.tex", width = 3.5, height = 2.2)
ggplot(data=head(data.set, 1), aes(x=factor(1), fill=Svar)) +
  geom_bar(aes(y=..count../sum(..count..)), position="dodge") +
  geom_bar(stat="identity", data=dist, aes(y=Sannolikhet), alpha=0, colour=alpha("black", 0.4)) +
  labs(y="Andel", x="Svar (1 obs)") + 
  coord_cartesian(ylim=c(0,1)) + 
  guides(fill = guide_legend(override.aes = list(colour = NULL))) +
  scale_fill_brewer("", limits=sizes,
                    type="div", palette=5, direction=1) + coord_flip() +
  theme(axis.text.y=element_blank(), legend.position="bottom")
dev.off()

tikz("median_data2.tex", width = 3.5, height = 2.2)
ggplot(data=head(data.set, 3), aes(x=factor(1), fill=Svar)) +
  geom_bar(aes(y=..count../sum(..count..)), position="stack") +
  geom_bar(stat="identity", data=dist, aes(y=Sannolikhet), alpha=0, colour=alpha("black", 0.4)) +
  labs(y="Andel", x="Svar (3 obs)") + 
  coord_cartesian(ylim=c(0,1)) + 
  guides(fill = guide_legend(override.aes = list(colour = NULL))) +
  scale_fill_brewer("", limits=sizes,
                    type="div", palette=5, direction=1) + coord_flip() +
  theme(axis.text.y=element_blank(), legend.position="bottom")
dev.off()

tikz("median_data3.tex", width = 3.5, height = 2.2)
ggplot(data=head(data.set, 8), aes(x=factor(1), fill=Svar)) +
  geom_bar(aes(y=..count../sum(..count..)), position="stack") +
  geom_bar(stat="identity", data=dist, aes(y=Sannolikhet), alpha=0, colour=alpha("black", 0.4)) +
  labs(y="Andel", x="Svar (8 obs)") + 
  coord_cartesian(ylim=c(0,1)) + 
  guides(fill = guide_legend(override.aes = list(colour = NULL))) +
  scale_fill_brewer("", limits=sizes,
                    type="div", palette=5, direction=1) + coord_flip() +
  theme(axis.text.y=element_blank(), legend.position="bottom")
dev.off()

tikz("median_data4.tex", width = 3.5, height = 2.2)
ggplot(data=head(data.set, 25), aes(x=factor(1), fill=Svar)) +
  geom_bar(aes(y=..count../sum(..count..)), position="stack") +
  geom_bar(stat="identity", data=dist, aes(y=Sannolikhet), alpha=0, colour=alpha("black", 0.4)) +
  labs(y="Andel", x="Svar (25 obs)") + 
  coord_cartesian(ylim=c(0,1)) + 
  guides(fill = guide_legend(override.aes = list(colour = NULL))) +
  scale_fill_brewer("", limits=sizes,
                    type="div", palette=5, direction=1) + coord_flip() +
  theme(axis.text.y=element_blank(), legend.position="bottom")
dev.off()

tikz("median_data5.tex", width = 3.5, height = 2.2)
ggplot(data=head(data.set, 100), aes(x=factor(1), fill=Svar)) +
  geom_bar(aes(y=..count../sum(..count..)), position="stack") +
  geom_bar(stat="identity", data=dist, aes(y=Sannolikhet), alpha=0, colour=alpha("black", 0.4)) +
  labs(y="Andel", x="Svar (100 obs)") + 
  coord_cartesian(ylim=c(0,1)) + 
  guides(fill = guide_legend(override.aes = list(colour = NULL))) +
  scale_fill_brewer("", limits=sizes,
                    type="div", palette=5, direction=1) + coord_flip() +
  theme(axis.text.y=element_blank(), legend.position="bottom")
dev.off()

sizenum <- c(1, 3, 10, 20, 30)
data.setnum <- data.frame(Svar = sizenum[as.integer(data.set$Svar)])

tikz("median_data5num.tex", width = 3.5, height = 2.2)
ggplot(data=head(data.setnum, 100), aes(x=factor(1), fill=ordered(Svar))) +
  geom_bar(aes(y=..count../sum(..count..)), position="stack") +
  geom_bar(stat="identity", data=dist, aes(y=Sannolikhet), alpha=0, colour=alpha("black", 0.4)) +
  labs(y="Andel", x="Svar (100 obs)") + 
  coord_cartesian(ylim=c(0,1)) + 
  guides(fill = guide_legend(override.aes = list(colour = NULL))) +
  scale_fill_brewer("", limits=sizenum,
                    type="div", palette=5, direction=1) + coord_flip() +
  theme(axis.text.y=element_blank(), legend.position="bottom")
dev.off()
