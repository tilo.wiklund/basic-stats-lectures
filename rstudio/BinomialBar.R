library(ggplot2)
library(tikzDevice)
options(tikzPdftexWarnUTF = F)
options(tikzDocumentDeclaration = "\\documentclass{beamer}")

binom <- function(n, p) dbinom(0:n, n, p)

tikz("binom1.tex", width=3, height=2)
ggplot(data=data.frame(x=0:10, y=binom(10, 0.2)), aes(x=x,y=y)) +
  geom_bar(stat="identity") +
  scale_x_discrete(limits=0:10) +
  labs(y = "Sannolikhet") +
  theme(axis.title.x = element_blank()) +
  scale_y_continuous(limits = c(0, 0.31))
dev.off()

tikz("binom2.tex", width=3, height=2)
ggplot(data=data.frame(x=0:10, y=binom(10, 0.5)), aes(x=x,y=y)) +
  geom_bar(stat="identity") +
  scale_x_discrete(limits=0:10) +
  labs(y = "Sannolikhet") +
  theme(axis.title.x = element_blank()) +
  scale_y_continuous(limits = c(0, 0.31))
dev.off()

tikz("binom3.tex", width=3, height=2)
ggplot(data=data.frame(x=0:10, y=binom(10, 0.8)), aes(x=x,y=y)) +
  geom_bar(stat="identity") +
  scale_x_discrete(limits=0:10) +
  labs(y = "Sannolikhet") +
  theme(axis.title.x = element_blank()) +
  scale_y_continuous(limits = c(0, 0.31))
dev.off()
